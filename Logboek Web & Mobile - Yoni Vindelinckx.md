# Logboek Web & Mobile - Yoni Vindelinckx
domeinnaam www.myhostingname.be


## datum (1 maart) 14u tot 16u - 2uur

- opzetten firebase


## datum (2 maart) 15u tot 18u - 3uur

- videos bekijken van firebase zelf

## datum (6 maart)  - 4uur 

- troubleshooting firebase init 
- home page html en css

## datum (23 maart) - 12u tot 14u - 2uur 

- login firebase met google account


## datum (16 april) - 9u tot 17u -  8uur 

- opzoeken documentatie firebase.

- firebaseUI en cards

## datum (17 april) - 12u tot 14u -  2uur 

- firestore documentatie nakijken

## datum (18 april) - 17u tot 21u - 4uur 

- broodjes toevoegen en ophalen uit firestore

## datum (19 april) - 14u tot 18u - 4uur 

- broodjes verwijderen en input fields vullen met de waarde van het boordje.


## datum (20 april) - 10u tot 15u -  5uur 

- videos bekijken van firebase best practices & verder werken aan update van broodjes.


## datum (21 april) - 9u tot 13u -  4uur 

- opzetten van bestellingen html en firebase.

- troubleshooting broodjes updaten functie


## datum (22 april) - 13u tot 20u -  7uur 

- troubleshooting store.js als winkelmandje gebruiken.

- informatie opzoeken en verschillende bronnen uitproberen.


## datum (23 april) - 9u tot 16u -  8uur 

- troubleshooting cordova firebaseUI, functies dat 2 keer worden uitgevoerd en toggle theme button werkt nog niet.


## datum (24 april)-  10u tot 17u - 8uur 

- broodjes bestellen en fix toggle theme.


## datum (25 april) - 10u tot 17u -  8uur 

- broodjes afronden, disable button en advanced broodjes samenstellen met input fields.


## datum (26 april) - 10u tot 17u -  8uur 

- doornemen van documentatie user roles & cloud messaging.


## datum (30 april) - 9u tot 13u -  4uur 

- troubleshooting image bij broodje, functie werkt pas na 2 keer duwen op add.


## datum (01 mei) - 17u tot 24u -  7uur 

- broodjes afronden, disable button, advanced broodjes samenstellen.


## datum (02 mei) - 9u tot 17u -  8uur 

- user informatie tonen in page & user informatie meersturen bij het bestellen van broodjes.

## datum (05 mei) - 13u tot 18u -  5uur 

- troubleshoot: images button moet 2 keer geklikt worden voor wanneer het werkt

- user broodjes op id tonen

- notification token aanmaken bij het inloggen

## datum (07 mei) - 14u tot 18u -  4uur 

- troubleshooting login voor cordova 

- troubleshooting notifications 

## datum (08 mei) - 14u tot 18u -  4uur 

- user roles in javascript, remove buttons bij role verschil 

- troubleshooting cloud function 

- troubleshooting notifications


## datum (10 mei) - 12u tot 22u -  10uur 

- UI mooier maken 

- firestore pagination 

- firestore rules 



## datum (12 mei) - 14u tot 18u -  4uur 

- troubleshooting firebaseUI cordova 

- ondeviceready event gebruiken



## datum (13 mei) - 14u tot 17u -  3uur 

- firestore rules aanpassen 

- details bewerken, code cleanup


## datum (14 mei) - 13u tot 21u -  8uur 

- details & app mooier maken & troubleshooting bepaalde problemen die ik nog niet had gezien.


## datum (15 mei) - 13u tot 15u -  2uur 

- details



## datum (16 mei) - 13u tot 15u -  2uur 

- update sandwiches laten werken en troubleshoot emulator fout 



## datum (26 mei) - 13u tot 16u -  3uur 

- release key aanmaken & troubleshoot release

## datum (27 mei) - 18u tot 20u -  2uur 

- troubleshoot onverwachts probleem met bestelling broodjes



## datum (6 juni- 12u tot 16u -  4uur 

- finetuning 




logboek: https://gitlab.com/yoniboy98/bronnen_mysandwich

